(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-agenda-files
   '("/home/voyager/org/courses/unix_linux.org" "/home/voyager/org/courses/cultural_geo.org" "/home/voyager/org/courses/c++.org" "/home/voyager/Documents/personal_notes/gratitude_journal.org" "/home/voyager/Documents/personal_notes/diary.org" "/home/voyager/org/courses/gov.org" "/home/voyager/org/courses/story_and_style.org" "/home/voyager/org/anime.org" "/home/voyager/org/braindump.org" "/home/voyager/org/diary.org" "/home/voyager/org/gnu+linux.org" "/home/voyager/org/gratitude.org" "/home/voyager/org/miscellaneous.org" "/home/voyager/org/org_testing.org" "/home/voyager/org/technical.org" "/home/voyager/org/todos.org"))
 '(package-selected-packages
   '(github-modern-theme github-dark-vscode-theme github-theme writeroom-mode vterm vscdark-theme pdf-tools org-superstar neotree masm-mode leetcode all-the-icons-ivy all-the-icons-dired)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-level-1 ((t (:inherit outline-1 :height 1.4))))
 '(org-level-2 ((t (:inherit outline-2 :height 1.3))))
 '(org-level-3 ((t (:inherit outline-3 :height 1.2))))
 '(org-level-4 ((t (:inherit outline-4 :height 1.1))))
 '(org-level-5 ((t (:inherit outline-5 :height 1.0)))))
